﻿/*
This information file is used by UUP dump downloader script and by its build
script.
*/

AppNameOnly = UUP dump downloader
CompanyName = UUP dump authors
Version     = 1.3.0-alpha.4

/*
Release type determines update check URL and if build metadata is shown in
version text displayed in application.

Possible values:
0 - Final release
1 - Testing release (alpha, beta, rc)
2 - Continous Integration (untested versions)
*/
ReleaseType = 2

/*
Enable to show PHP console during run time

0 - Disabled
1 - Enabled
*/
Debug = 0

;These values are there only, because they need to be used by build script
AppFileName = uupdownloader_%Version%
UserAgent   = %AppNameOnly%/%Version%
Copyright   = © %A_YYYY% %CompanyName%

;Version number without build metadata generation part
VersionNoMeta := StrSplit(Version, "+")
VersionNoMeta := VersionNoMeta.1

If(ReleaseType >= 2) {
    AppName = %AppNameOnly% v%Version%
} else {
    AppName = %AppNameOnly% v%VersionNoMeta%
}
